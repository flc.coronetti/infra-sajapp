#!/bin/bash
# chmod +x configure_master.sh
# ./configure_master.sh IPLAN
# ex: ./configure_master.sh 192.168.1.16

kubeadm init --apiserver-advertise-address "$1"

mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"


sudo apt install -y bash-completion
source <(kubectl completion bash) # configuração de autocomplete no bash do shell atual, o pacote bash-completion precisa ter sido instalado primeiro.
echo "source <(kubectl completion bash)" >> ~/.bashrc # para adicionar o autocomplete permanentemente no seu shell bash.
Necessário quando ocorre erro para autocompletar e deletar pods, etc.
source /etc/bash_completion
echo "source /etc/bash_completion" >> ~/.bashrc

kubectl get nodes
