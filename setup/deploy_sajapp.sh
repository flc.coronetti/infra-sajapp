#!/bin/bash
# chmod +x deploy_sajapp.sh
# ./deploy_sajapp.sh

# Namespaces
kubectl create -f https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/namespaces/tools.yaml --save-config --record

# Limits
kubectl create -f https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/limits/constraints.yaml --save-config --record

# Autoscaler
kubectl create -f https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/autoscaler/hpa.yaml --save-config --record

# Deployments
kubectl create -f https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/deployments/sajapp.yaml --save-config --record

# Services
kubectl create -f https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/services/sajapp.yaml --save-config --record
