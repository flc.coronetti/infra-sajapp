#!/bin/bash
# chmod +x install_dependencies.sh
# ./install_dependencies.sh

# Disable swap
swapoff -a
sed -i '/swap/s/^/#/' /etc/fstab

# Install docker
apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
apt-get install -y docker-ce=5:18.09.1~3-0~ubuntu-bionic docker-ce-cli=5:18.09.1~3-0~ubuntu-bionic containerd.io
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

# Install k8s
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
apt-get -y update
apt-get install -y kubelet kubeadm kubectl
