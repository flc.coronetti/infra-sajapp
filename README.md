# infra-sajapp

### **Objetivo:**
Demonstrar a configuração do cluster kubernetes e realizar o deploy da aplicação Saj(nginx).

### **Requisitos:**
- OS: Ubuntu 18.04 Server;
- 3 servidores com no mínimo 2xCPU e 4GBxMemória;
- Os servidores já devem estar com IP estático configurado e com um nome distinto;

Sugestão de nomes:
- k8s1 - master
- k8s2 - worker
- k8s3 - worker

### **Exigência do projeto:**
- A aplicação deverá ser executada no namespace "tools;
- Deverá possuir no mínimo 3 e máximo 5 réplicas;
- O limite de CPU não pode ser superior a 2cores.
- O limite de Memória não pode ser superior a 4GB.

### **Instalação**

Instalação de dependências:

Acesse via ssh o host que deverá compor o cluster
```ssh -p 22 usuario@k8s1```

Use o comando abaixo para logar como root:
`sudo su`

Execute os comandos abaixo para baixar e executar o script para instalar as dependencias:
```
curl -fsSL https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/setup/install_dependencies.sh -o install_dependencies.sh
sh install_dependencies.sh
```


Execute o procedimento a cima em todos os hosts.

### **Configuração do Master**

Acesse via ssh o host Master que deverá compor o cluster:
`ssh -p 22 usuario@k8s1`

Use o comando abaixo para logar como root:
`sudo su`

`curl -fsSL https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/setup/configure_master.sh -o configure_master.sh`
Execute o script com o IP de lan do servidor como parametro:
`sh configure_master.sh 192.168.1.23`

Ao final da execução copie o comando do kubeadm para poder adicionar os workers no cluster:
Exemplo de saída:
```
kubeadm join 192.168.1.20:6443 --token pzs9qm.1iha9g7x2p3apnal \
    --discovery-token-ca-cert-hash sha256:f533555c6081128558fe3e90e68da0c19387baaa62eb99ed8e8519edc6f05fee
```
 

### **Configuração dos Workers**

Este prodecimento deve ser executado somente nos workers, no nosso caso o k8s2 e k8s3:
Acesse via ssh o host Master que deverá compor o cluster:
`ssh -p 22 usuario@k8s2`

Use o comando abaixo para logar como root:
`sudo su`

Execute o comando conforme copiado da saída da configuração do master, exemplo:
```
kubeadm join 192.168.1.20:6443 --token pzs9qm.1iha9g7x2p3apnal \
    --discovery-token-ca-cert-hash sha256:f533555c6081128558fe3e90e68da0c19387baaa62eb99ed8e8519edc6f05fee 
```


### **Deploy da infraestrutura da aplicação**

Este procecimento deve ser executado no Master (k8s1):
Acesse via ssh o host Master que deverá compor o cluster:
`ssh -p 22 usuario@k8s2`

Use o comando abaixo para logar como root:
`sudo su`

Baixe e execute o script para realizar o deploy da aplicação:
```
curl -fsSL https://gitlab.com/flc.coronetti/infra-sajapp/-/raw/master/setup/deploy_sajapp.sh -o deploy_sajapp.sh
sh deploy_sajapp.sh
```

Verificando se tudo deu certo
```
root@k8s1:/home/k8s# kubectl get all -n tools -o wide
NAME                           READY   STATUS    RESTARTS   AGE     IP          NODE   NOMINATED NODE   READINESS GATES
pod/saj-app-76c56fc5c8-9n4k7   1/1     Running   0          8m58s   10.46.0.2   k8s3   <none>           <none>
pod/saj-app-76c56fc5c8-jtfz5   1/1     Running   0          8m58s   10.46.0.1   k8s3   <none>           <none>
pod/saj-app-76c56fc5c8-vp72d   1/1     Running   0          8m57s   10.46.0.3   k8s3   <none>           <none>

NAME              TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE    SELECTOR
service/saj-app   NodePort   10.97.115.161   <none>        80:30003/TCP   4h9m   app=saj-app

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE     CONTAINERS   IMAGES         SELECTOR
deployment.apps/saj-app   3/3     3            3           8m58s   saj-app      nginx:1.19.7   type=saj

NAME                                 DESIRED   CURRENT   READY   AGE     CONTAINERS   IMAGES         SELECTOR
replicaset.apps/saj-app-76c56fc5c8   3         3         3       8m58s   saj-app      nginx:1.19.7   pod-template-hash=76c56fc5c8,type=saj

NAME                                          REFERENCE            TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/saj-app   Deployment/saj-app   <unknown>/50%   3         5         3          4h11m
```

Verifique note que os nós estão executando no node k8s3.

Podemos acessar a aplicação usando o IP do host k8s3 da segunte forma:
`http://192.168.1.25:30003`

**OBS**: Para esta demonstração estamos acessando diretamente o host, o que não é o ideal, o recomendado seria utilizarmos um increase juntamente com um load balancer.

## **Questão 2**
Para obtermos acesso aos logs dos containers e do cluster de forma centralizada podemos configurar o servidor do ElasticSearch, conforme documentação oficial.

Link: https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-beat-configuration-examples.html
